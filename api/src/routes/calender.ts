import { Router, Request, Response } from 'express';
import { CalenderService } from '../services/calenderService';
import jwt from 'jsonwebtoken';
import moment, { Moment } from 'moment';

const router: Router = Router();
const calenderService = new CalenderService();

router.get('/', async (req: Request, res: Response) => {
  let start: Moment = moment(req.query.start, 'YYYY-MM-DD');
  let end: Moment = moment(req.query.end, 'YYYY-MM-DD');
  if (end.toISOString() < start.toISOString()) {
    res.sendStatus(400);
  } else {
    let token = <{ [key: string]: any }>jwt.decode(
      res.locals.token.replace(new RegExp('(bearer )', 'gi'), '')
    );
    let raplaUrl = token['raplaServer'] + '?key=' + token['raplaKey'];

    res.json(await calenderService.getLectures(raplaUrl, start, end));
  }
});

export const CalenderController: Router = router;
