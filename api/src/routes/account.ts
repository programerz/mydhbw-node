import { Router, Request, Response } from 'express';
import { AccountService } from '../services/accountService';
import { Login } from '../models/login';

const router: Router = Router();
const config = require('../config');
const accountService: AccountService = new AccountService();

router.post('/login', async (req: Request, res: Response) => {
  let tmp: Login = req.body;
  if (tmp.password === config.users[tmp.username].password) {
    res.json(await accountService.generateTokens(tmp.username));
  }
  res.sendStatus(400);
});
router.post('/logout', async (req: Request, res: Response) => {
  let tmp: {
    token: string;
    expiration: Date;
  } =
    req.body;

  await accountService.logout(tmp.token);
  res.sendStatus(200);
});
router.post('/token/renew', async (req: Request, res: Response) => {
  let body: {
    token: string;
    expiration: Date;
  } =
    req.body;
  let tmp = await accountService.renewToken(body);
  if (tmp) {
    res.json(tmp);
  }
  res.sendStatus(400);
});
router.post('/register', (req: Request, res: Response) => {
  res.sendStatus(404);
});

export const AccountController: Router = router;
