import express from 'express';
import bodyparser from 'body-parser';

import {
  CalenderController,
  AccountController
} from './routes';

import { AccountService } from './services/accountService';

const app: express.Application = express();
const port: number = 8080;
const contextPath: string = '/api';
const accountService = new AccountService();

app.use(contextPath, (req, res, next) => {
  let authorized = false;
  let tmp = <string>req.header('Authorization');
  if (req.url != '/account/login' && req.url != '/account/token/renew') {
    if (tmp) {
      if (
        accountService.validateToken(
          tmp.replace(new RegExp('(bearer )', 'gi'), '')
        )
      ) {
        authorized = true;
      }
    }
  } else {
    authorized = true;
  }
  if (authorized) {
    res.locals.token = tmp;
    next();
  } else {
    console.log(`Unauthorized request from ${req.ip}`);
    res.sendStatus(401);
  }
});

app.use(bodyparser.json());

app.use(contextPath + '/rapla/fullcalendar', CalenderController);
app.use(contextPath + '/account', AccountController);

app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}/`);
});
