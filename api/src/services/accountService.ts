import jwt from 'jsonwebtoken';
import uuid from 'uuid';
import { Tokens } from '../models/tokens';
import { ExpirationStrategy, MemoryStorage } from 'node-ts-cache';

export class AccountService {
  config = require('../config');
  cache = new ExpirationStrategy(new MemoryStorage());
  constructor() {}

  /**
   * Generates the access and the refresh token for the authenticated User.
   *
   * @param {string} name The name of the user.
   * @returns {Tokens} The generated tokens.
   * @memberof AccountService
   */
  async generateTokens(name: string): Promise<Tokens> {
    let access = {
      token: jwt.sign(
        {
          raplaServer: this.config.users[name].raplaServer,
          raplaKey: this.config.users[name].raplaKey
        },
        this.config.Tokens.Key,
        {
          expiresIn: 10 * 60,
          algorithm: 'HS256',
          subject: name,
          jwtid: uuid.v4()
        }
      ),
      expiration: new Date(Date.now() + 10 * 60000)
    };
    let refresh = {
      token: uuid.v4().replace(new RegExp('-', 'g'), ''),
      expiration: new Date(Date.now() + 12 * 60 * 60000)
    };
    await this.cache.setItem(refresh.token, name, {
      ttl: (refresh.expiration.getTime() - Date.now()) / 1000,
      isLazy: false
    });
    return { access, refresh };
  }

  /**
   *
   *
   * @param {{ token: string; expiration: Date }} refresh
   * @returns
   * @memberof AccountService
   */
  async renewToken(refresh: { token: string; expiration: Date }) {
    let tmp = await this.cache.getItem<string>(refresh.token);
    await this.cache.setItem(refresh.token, null, { ttl: 1, isLazy: false });
    if (tmp) {
      return this.generateTokens(tmp);
    }
  }

  /**
   *
   *
   * @param {string} key
   * @memberof AccountService
   */
  async logout(key: string) {
    //delete the element from the cache using the ttl: 1 hack
    await this.cache.setItem(key, null, { ttl: 1, isLazy: false });
  }

  /**
   *
   *
   * @memberof AccountService
   */
  registerUser() {
    console.log('not implemented yet');
  }

  /**
   *
   *
   * @param {string} access
   * @returns {boolean}
   * @memberof AccountService
   */
  validateToken(access: string): boolean {
    try {
      jwt.verify(access, this.config.Tokens.Key,{ignoreExpiration: false});
    } catch {
      return false;
    }
    return true;
  }
}
