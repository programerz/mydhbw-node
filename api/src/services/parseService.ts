import { Lecture } from '../models/lecture';
import axios from 'axios';
import cheerio from 'cheerio';
import moment, { Moment } from 'moment';
import { XmlEntities } from 'html-entities';

export class ParseService {
  constructor() {}

  private xml: XmlEntities = require('html-entities').XmlEntities;

  async getLecturesPerWeek(
    raplaUrl: string,
    monday: Moment
  ): Promise<Lecture[]> {
    const reqUrl = `${raplaUrl}&day=${monday.date()}&month=${monday.month() +
      1}&year=${monday.year()}`;
    const events: Lecture[] = [];

    await axios
      .get(reqUrl)
      .then(async res => {
        const entities = cheerio.load(res.data, { decodeEntities: false });

        await entities('td.week_block').each(async (ind, ele) => {
          let tmp = await this.parseLecture(<CheerioElement>ele, monday);
          events.push(tmp);
        });
        // TODO: Error Handling
      })
      .catch(error => {
        console.log(error);
      });
    return events;
  }

  private async parseLecture(
    block: CheerioElement,
    monday: Moment
  ): Promise<Lecture> {
    const title = this.xml.decode(
      cheerio(block)
        .find('a')
        .html()!
        .match(new RegExp('<br>' + '(.*)' + '<span class="tooltip">'))![1]
    );
    let duration = this.xml.decode(
      cheerio(block)
        .find('a > span.tooltip > div')
        .eq(1)
        .html()!
    );

    try {
      let startDate: Moment;
      let endDate: Moment;

      if (
        new RegExp('([0-9]{2}[.][0-9]{2}[.][0-9]{2})').test(
          duration.split(' ')[1]
        )
      ) {
        const date = duration.split(' ')[1];

        const startTime = duration.split(' ')[2].split('-')[0];
        const endTime = duration.split(' ')[2].split('-')[1];

        startDate = moment(`${date} ${startTime}`, 'DD.MM.YYYY HH:mm');
        endDate = moment(`${date} ${endTime}`, 'DD.MM.YYYY HH:mm');
      } else {
        const day = duration.split(' ')[0];

        const startTime: string = duration!.split(' ')[1].split('-')[0];
        const endTime = duration.split(' ')[1].split('-')[1];
        let offset = 0;
        switch (day) {
          case 'Mo':
            break;
          case 'Di':
            offset = 1;
            break;
          case 'Mi':
            offset = 2;
            break;
          case 'Do':
            offset = 3;
            break;
          case 'Fr':
            offset = 4;
            break;
          case 'Sa':
            offset = 5;
            break;
          case 'So':
            offset = 6;
            break;

          default:
            console.log(`Error Parsing Rapla for ${title}!`);

            break;
        }
        startDate = moment(
          `${monday.format('DD.MM.YYYY')} ${startTime}`,
          'DD.MM.YYYY HH:mm'
        ).add(offset, 'd');
        endDate = moment(
          `${monday.format('DD.MM.YYYY')} ${endTime}`,
          'DD.MM.YYYY HH:mm'
        ).add(offset, 'd');
      }

      let persons: string[] = [];
      cheerio(block)
        .find('span.person')
        .each((ind, ele) => {
          persons.push(cheerio(ele).html()!);
        });

      const resources: string[] = [];

      cheerio(block)
        .find('span.resource')
        .each((ind, ele) => {
          resources.push(cheerio(ele).html()!);
        });

      return {
        title: title,
        start: startDate!.format('YYYY-MM-DDTHH:mm:ss'),
        end: endDate!.format('YYYY-MM-DDTHH:mm:ss'),
        lecturers: persons,
        resources: resources,
        editable: false
      };
    } catch (err) {
      console.warn(`Error parsing rapla in week of ${monday}!`);
      return {
        title: 'Error Parsing Rapla',
        start: moment.max().format('YYYY-MM-DDTHH:mm:ss'),
        end: moment.max().format('YYYY-MM-DDTHH:mm:ss'),
        lecturers: ['-'],
        resources: ['-'],
        editable: false
      };
    }
  }
}
