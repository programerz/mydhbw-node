import { Lecture } from '../models/lecture';
import { ExpirationStrategy, MemoryStorage } from 'node-ts-cache';
import { ParseService } from './parseService';
import moment, { Moment } from 'moment';

export class CalenderService {
  cache = new ExpirationStrategy(new MemoryStorage());
  parseService = new ParseService();
  constructor() {}

  /**
   * Retrieves a Array of Lectures between start and end date.
   *
   * @param {string} raplaUrl The URL of the rapla Server.
   * @param {Moment} startDate The start date.
   * @param {Moment} endDate The end date.
   * @returns {Promise<Lecture[]>} Array of Lectures.
   * @memberof LectureService
   */
  async getLectures(
    raplaUrl: string,
    startDate: Moment,
    endDate: Moment
  ): Promise<Lecture[]> {
    let tmp: Lecture[] = [];
    let weeks = this.getWeeks(startDate, endDate);
    for (let element of weeks) {
      let lectures: Lecture[];

      lectures = await this.cache.getItem<Lecture[]>(element.toISOString());
      if (!lectures) {
        console.log(`Cache miss for week from ${element.format('YYYY-MM-DD')}`);

        lectures = await this.parseService.getLecturesPerWeek(
          raplaUrl,
          element
        );
        this.cache.setItem(element.toISOString(), lectures, {
          ttl: 43200,
          isLazy: false
        });
      }
      tmp = tmp.concat(lectures);
    }
    tmp.filter(element => {
      moment(element.start, 'YYYY-MM-DDTHH:mm:ss') <= startDate &&
        moment(element.end, 'YYYY-MM-DDTHH:mm:ss') >= endDate;
    });

    return tmp;
  }

  private getWeeks(startDate: Moment, endDate: Moment): Moment[] {
    let diff = -startDate.diff(endDate, 'week');
    let startDay = startDate.subtract(startDate.weekday() - 1, 'd');

    let tmp: Moment[] = [];

    tmp.push(moment(startDay));

    for (let i = 0; i < diff; i++) {
      tmp.push(moment(startDay.add(7, 'days')));
    }
    return tmp;
  }
}
