export class Tokens {
  access!: {
    token: string;
    expiration: Date;
  };

  refresh!: {
    token: string;
    expiration: Date;
  };
}
