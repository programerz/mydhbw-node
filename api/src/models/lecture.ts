import { Moment } from 'moment';

export class Lecture {
  editable!: boolean;
  title!: string;
  start!: string;
  end!: string;
  resources!: string[];
  lecturers!: string[];
}
