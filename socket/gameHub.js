var exports = (module.exports = {});

var jwt = require('jsonwebtoken');
var gameService = require('./gameService');

var config = require('./config');

gameGroups = [];

gameSockets = [];

exports.connect = function(socket, game) {
  try {
    gameSockets.push(socket);
    //decode JWT
    decodedGroupName = jwt.verify(
      socket.handshake.query.token,
      config.Tokens.Key
    );

    var groupObject = addGroupOIfNotExisting(decodedGroupName.sub, gameGroups);

    // User joining lobby
    socket.on('EnterLobby', function(data) {
      console.log(data + ' joined the GameLobby');
      groupObject.users.push(new Player(data, socket.id));
      game.emit('SetPlayersOnline', groupObject.users);
    });

    //Disconnect
    socket.on('disconnect', function(data) {
      gameSockets.splice(gameSockets.indexOf(socket), 1);
      for (var i = 0; i < groupObject.users.length; i++) {
        if (groupObject.users[i].id == socket.id) {
          console.log(groupObject.users[i].name + ' left the Game');
          groupObject.users.splice(i, 1);
        }
      }
      game.emit('SetPlayersOnline', groupObject.users);
    });

    // Send game request
    socket.on('SendGameRequest', function(data) {
      console.log(data.from.name + ' send Request to ' + data.to.name);
      game.to(data.to.id).emit('RecieveRequest', data);
    });

    //Start Game
    socket.on('AcceptGameRequest', function(data) {
      matchid = data.from.id + data.to.id;

      // join Game
      socket.join(matchid);
      gameSockets.forEach(socketFrom => {
        if (socketFrom.id == data.from.id) {
          socketFrom.join(matchid);
        }
      });
      match = gameService.addGame(data, matchid);
      game.in(matchid).emit('StartGame', match);
    });

    socket.on('SendMove', function(data) {
      match = data;
      match.turn = !match.turn;
      match = gameService.processMatch(match);
      game.in(match.matchId).emit('SetNewBoard', match);
    });

    socket.on('EndGame', function(data) {
      match = data;
      +game.in(match.matchId).emit('EndGame', match);

      gameSockets.forEach(socketFrom => {
        if (
          socketFrom.id == match.playerX.id ||
          socketFrom.id == match.playerO.id
        ) {
          socketFrom.leave(match.matchid);
        }
      });
      gameService.removeGame(match);
    });
  } catch (error) {
    socket.disconnect();
  }
};

function addGroupOIfNotExisting(name, groupArray) {
  //serach if group exists
  var found = false;
  for (var i = 0; i < groupArray.length; i++) {
    if (groupArray[i].name == name) {
      // group existing

      found = true;

      return groupArray[i];
      break;
    }
  }
  // group doesn't exists
  if (!found) {
    var group = new Group(name);

    groupArray.push(group);
    return group;
  }
}

class Player {
  constructor(name, id) {
    this.name = name;
    this.id = id;
  }
}

class Request {
  constructor(from, to) {
    this.from = from;
    this.to = to;
  }
}

class Group {
  constructor(name) {
    this.name = name;
    this.users = [];
  }
}
