var exports = module.exports = {};

var games = [];

var combos =
    [['1', '2', '3'],
    ['4', '5', '6'],
    ['7', '8', '9'],
    ['1', '4', '7'],
    ['2', '6', '8'],
    ['3', '6', '9'],
    ['3', '5', '7'],
    ['1', '5', '9']];

exports.addGame = function (request, matchid) {
    match = new Match(matchid, request.from, request.to, true);
    games.push(match);
    return match;
};



exports.processMatch = function (match) {


    if (checkWin(match, "X")) {
        match.xwin++;
        match.board = [null, null, null, null, null, null, null, null, null];
    }
    if (checkWin(match, "O")) {
        match.owin++;
        match.board = [null, null, null, null, null, null, null, null, null];
    }
    if (checkDraw(match)) {
        match.board = [null, null, null, null, null, null, null, null, null];
        match.draw++;
    }

    return match;
}


exports.removeGame = function (match) {

    // todo
}

function checkWin(match, player) {
    win = false;

    combos.forEach(combo => {
        if (match.board[combo[0] - 1] == player && match.board[combo[1] - 1] == player && match.board[combo[2] - 1] == player) {
            win = true;
        }

    });
    return win;
}

function checkDraw(match) {
    counter = 0;
    match.board.forEach(field => {
        if (field != null) {
            counter++;
        }
    })
    if (counter == 9) {
        return true;
    } else { return false; }

}

class Match {
    constructor(matchId, playerX, playerO, turn) {
        this.matchId = matchId;
        this.playerX = playerX;
        this.playerO = playerO;
        this.turn = turn;
        this.board = [null, null, null, null, null, null, null, null, null];
        this.xwin = 0;
        this.owin = 0;
        this.draw = 0;
    }
}