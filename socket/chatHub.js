var exports = (module.exports = {});

var jwt = require('jsonwebtoken');

var config = require('./config');

chatGroups = [];

exports.connect = function(socket, chat) {
  var groupObject;
  var decodedGroupName;
  try {
    //decode JWT
    decodedGroupName = jwt.verify(
      socket.handshake.query.token,
      config.Tokens.Key
    );

    // join this group
    socket.join(decodedGroupName.sub);

    var groupObject = addGroupOIfNotExisting(decodedGroupName.sub, chatGroups);

    // Emit Online UserList to current socket
    chat
      .in(decodedGroupName.sub)
      .emit('SetUsersOnline', generateUserArray(groupObject.users));

    // ON USER JOIN setting SETUSERONLINELIST
    socket.on('UserJoin', function(data) {
      console.log(data + ' joined the Chat');
      groupObject.users.push(new User(data, socket.id));
      chat
        .in(decodedGroupName.sub)
        .emit('SetUsersOnline', generateUserArray(groupObject.users));
    });

    // Recieve message
    socket.on('SendMessage', function(data) {
      data.time = new Date()
        .toISOString()
        .replace(/T/, ' ')
        .replace(/\..+/, '');

      //sending message to connected users
      chat.in(groupObject.name).emit('ReceiveMessage', data);
    });

    //Disconnect
    socket.on('disconnect', function(data) {
      for (var i = 0; i < groupObject.users.length; i++) {
        if (groupObject.users[i].id == socket.id) {
          console.log(groupObject.users[i].name + ' left the Chat');
          groupObject.users.splice(i, 1);
        }
      }
      chat
        .in(decodedGroupName.sub)
        .emit('SetUsersOnline', generateUserArray(groupObject.users));
    });
  } catch (err) {
    socket.disconnect();
  }
};

function addGroupOIfNotExisting(name, groupArray) {
  //serach if group exists
  var found = false;
  for (var i = 0; i < groupArray.length; i++) {
    if (groupArray[i].name == name) {
      // group existing

      found = true;

      return groupArray[i];
      break;
    }
  }
  // group doesn't exists
  if (!found) {
    var group = new Group(name);

    groupArray.push(group);
    return group;
  }
}

function generateUserArray(users) {
  userStringArray = [];
  for (var i = 0; i < users.length; i++) {
    userStringArray.push(users[i].name);
  }

  return userStringArray;
}

class Group {
  constructor(name) {
    this.name = name;
    this.users = [];
  }
}

class User {
  constructor(name, id) {
    this.name = name;
    this.id = id;
  }
}
