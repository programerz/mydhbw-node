var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var jwt = require('jsonwebtoken');

var chatHub = require('./chatHub');

var gameHub = require('./gameHub');

var chat = io.of('/chat');
var game = io.of('/game');

var port = process.env.PORT || 8080;
server.listen(port);
console.log('Server running ' + port);

chat.on('connect', function (socket) {
    chatHub.connect(socket, chat);
})


game.on('connect', function (socket) {
    gameHub.connect(socket, game);
})
