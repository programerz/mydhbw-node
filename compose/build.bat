@echo off
docker build --rm -t "registry.gitlab.com/programerz/mydhbw-node/reverse" ../reverseProxy/
docker build --rm -t "registry.gitlab.com/programerz/mydhbw-node/filemanagement" ../fileManagement/
docker build --rm -t "registry.gitlab.com/programerz/mydhbw-node/frontend" ../frontend/
docker build --rm -t "registry.gitlab.com/programerz/mydhbw-node/api" ../api/
docker build --rm -t "registry.gitlab.com/programerz/mydhbw-node/socket" ../socket/