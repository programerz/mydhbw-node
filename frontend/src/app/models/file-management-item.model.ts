export class FileManagementItem {
  constructor() {

  }

  name: string;
  extension: string;
  path: string;
  size: number;

  public static isFolder(item: FileManagementItem): boolean {
    if (item.extension != "/") {
      return false;
    }
    else {
      return true;
    }
  }

  public static getPath(item: FileManagementItem): string {
    if (this.isFolder(item)) {
      if(item.path == "")
      {
        return item.name + item.extension;
      }
      else{
        return item.path + "/" +item.name + item.extension;
      }

    }
    else {
      return item.path + "/" + item.name + "."+ item.extension;
    }
  }

  public static getSize(item: FileManagementItem): string {
    if (this.isFolder(item)) {
      if (item.size == 0) {
        return 'Leer';
      }
      else if (item.size == 1) {
        return item.size + " Datei";
      }
      else {
        return item.size + " Dateien";
      }
    }
    else {

      var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
      if (item.size == 0) return '0 Bytes';
      var i = Math.floor(Math.log(item.size) / Math.log(1024));
      return Math.round(item.size / Math.pow(1024, i)) + ' ' + sizes[i];
    }
  }
}
