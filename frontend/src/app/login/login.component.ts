import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../authentication/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  loading = false;
  returnUrl: string;

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private authService: AuthService) { }

  ngOnInit() {
      // reset login status
      if(this.authService.CurrentRefreshToken != null)
      {
      this.authService.logout();
      }

      // get return url from route parameters or default to '/'
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  async login() {
      this.loading = true;
      await this.authService.login(this.model.username, this.model.password);

      if(this.authService.CurrentAccessToken != null && this.authService.CurrentRefreshToken != null)
      {
        this.router.navigate([this.returnUrl]);
      }
      else{
        this.loading = false;
      }
  }
}
