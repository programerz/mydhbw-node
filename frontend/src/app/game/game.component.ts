import { Component, AfterViewInit, NgZone } from "@angular/core";
import { ConfigService } from "../config.service";
import { AuthService } from "../authentication/auth.service";
import { Router, RoutesRecognized, Event } from "@angular/router";

import * as io from "socket.io-client";

@Component({
  selector: "app-game",
  templateUrl: "./game.component.html",
  styleUrls: ["./game.component.scss"]
})
export class GameComponent implements AfterViewInit {
  hubConnected = false;
  //NgZone for forcing the UI to update.
  zone: NgZone;

  //Boolean Values to control displayed content.
  isEnterScreenHidden = false;
  isPlayerSelectionHidden = true;
  isGameHidden = true;

  //Models and fields needed for the actual Game.
  playerList = new Array<Player>();
  requestList = new Array<Request>();
  local: Player;
  game: Game = new Game();

  private socket: SocketIOClient.Socket;

  baseURL: string;

  constructor(
    router: Router,
    private configService: ConfigService,
    private authService: AuthService
  ) {
    this.zone = new NgZone({ enableLongStackTrace: false });
    router.events.subscribe(event => {
      if (event instanceof RoutesRecognized) {
        if (this.hubConnected) {
          this.hubConnected = false;
          this.socket.disconnect();
        }
      }
    });
  }

  async ngAfterViewInit() {
    if (this.authService.CurrentAccessToken == null) {
      await this.authService.newRefreshAccessToken().toPromise();
    }

    this.baseURL = this.configService.RootUrl + "/game" + "?token=";
    this.baseURL = this.baseURL + this.authService.CurrentAccessToken.token;

    this.startConnection();
    this.registerOnServerEvents();
    // this.socket.onclose(k => {
    //   this.ngAfterViewInit();
    // });


  }

  private registerOnServerEvents(): void {
    this.socket.on("SetPlayersOnline", (data: Array<Player>) => {
      this.zone.run(() => {
        if (this.local != null) {
          this.playerList = data.filter(player => player.id != this.local.id);
        }
      });
    });

    this.socket.on("RecieveRequest", (data: Request) => {
      this.zone.run(() => {
        if (
          this.requestList.filter(request => (request.from = data.from))
            .length == 0
        ) {
          this.requestList.push(data);
        }
      });
    });

    this.socket.on("StartGame", (data: Game) => {
      this.zone.run(() => {
        this.game = data;
        this.isPlayerSelectionHidden = true;
        this.isGameHidden = false;
      });
    });

    this.socket.on("SetNewBoard", (data: Game) => {
      this.zone.run(() => {
        this.game = data;
      });
    });

    this.socket.on("EndGame", () => {
      this.zone.run(() => {
        this.game = new Game();
        this.isGameHidden = true;
        this.isPlayerSelectionHidden = false;
      });
    });
  }

  private startConnection(): void {
    try {
      this.socket = io(this.baseURL);

      console.log("Hub connection started");
      this.hubConnected = true;
    } catch (error) {
      console.log("Error while establishing connection");
    }
  }

  enterlobby(name: string) {
    this.socket.emit("EnterLobby", name);
    this.local = new Player(name, this.socket.id);
    this.isEnterScreenHidden = true;
    this.isPlayerSelectionHidden = false;
  }

  sendRequest(to: Player) {
    let tmp = new Request(this.local, to);
    this.socket.emit("SendGameRequest", tmp);
  }

  acceptRequest(request: Request) {
    this.requestList = this.requestList.filter(tmp => tmp.from != request.from);
    this.socket.emit("AcceptGameRequest", request);
  }

  declineRequest(request: Request) {
    this.requestList = this.requestList.filter(tmp => tmp.from != request.from);
  }

  sendMove(position: number) {
    if (this.game.board[position] == null) {
      if (this.game.turn && this.local.id == this.game.playerX.id) {
        this.game.board[position] = "X";
        this.socket.emit("SendMove", this.game);
      } else if (!this.game.turn && this.local.id == this.game.playerO.id) {
        this.game.board[position] = "O";
        this.socket.emit("SendMove", this.game);
      }
    }
  }

  endGame() {
    this.socket.emit("EndGame", this.game);
  }
}

class Player {
  constructor(name: string, id: string) {
    this.name = name;
    this.id = id;
  }
  name: string;
  id: string;
}

class Request {
  constructor(from: Player, to: Player) {
    this.from = from;
    this.to = to;
  }
  from: Player;
  to: Player;
}

class Game {
  matchId: string;
  playerX: Player;
  playerO: Player;
  turn: boolean;
  board: Array<string> = new Array<string>(9);
  xwin: number = 0;
  owin: number = 0;
  draw: number = 0;
}
