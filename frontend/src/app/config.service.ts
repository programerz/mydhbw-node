import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';

@Injectable()
export class ConfigService {

  constructor() { }

  get RootUrl() {
    if (environment.spahost) {
      return "";
    }
    else {
      //return "http://localhost:61235/"
      //return "http://localhost:4300/";
      return "https://dhbw-wbetwo-backend-dev.azurewebsites.net/";
    }
  }

}
