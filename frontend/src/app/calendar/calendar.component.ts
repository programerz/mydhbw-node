import { Component, AfterViewInit } from '@angular/core';
import { AuthService } from '../authentication/auth.service';
import { ConfigService } from '../config.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements AfterViewInit {

  constructor(private authService: AuthService, private config: ConfigService) {
  }

  ngAfterViewInit() {
    var self = this;
    $('#raplaCalendar').fullCalendar(this.GetCalendarOptions());
  }
  private GetCalendarOptions() {
    var self = this;
    let calendarOptions: Object =
      {
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay,listWeek'
        },
        themeSystem: 'bootstrap3',
        navLinks: true, // can click day/week names to navigate views
        locale: 'de',
        weekNumbers: true,
        weekNumbersWithinDays: true,
        weekNumberCalculation: 'ISO',

        defaultView: 'agendaWeek',


        minTime: '08:00:00',
        maxTime: '18:00:00',
        weekends: false,

        height: 'auto',
        contentHeight: 'auto',

        editable: false,
        eventLimit: 3,
        eventBackgroundColor: '#2d4155',
        eventBorderColor: '#2d4155',
        eventTextColor: 'white',
        events: {
          url: self.config.RootUrl + 'api/rapla/fullcalendar',
          beforeSend: async function (xhr) {
            console.log("Calender Sync");

            if (self.authService.CurrentAccessToken == null) {
              await self.authService.newRefreshAccessToken().toPromise();
            }
            if (self.authService.CurrentAccessToken != null) {
              xhr.setRequestHeader('Authorization', 'bearer ' + self.authService.CurrentAccessToken.token);
            }
          },
          success: function () { console.log("Calender Sync End"); },
          error: function() {$('#raplaCalendar').fullCalendar( 'refetchEvents' );}
        },
        eventRender: function (event, element, view) {

          switch (view.name) {
            case 'listWeek':
              break;
            case 'month':
              break;
            default:
              event.resources = event.resources.sort();
              for (var v in event.resources) // for acts as a foreach
              {
                element.append("<div class='desc' style='color: rgba(255, 255, 255, 0.5);'>" + event.resources[v]) + "</div>";
              }
              for (var v in event.lecturers) // for acts as a foreach
              {
                element.append("<div class='desc' style='color: rgba(255, 255, 255, 0.5);'>" + event.lecturers[v]) + "</div>";
              }
          }

        }
      }

    return calendarOptions;
  }
}




