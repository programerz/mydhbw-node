import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private inj: Injector) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const auth = this.inj.get(AuthService);

    if (request.url.includes(auth.TokenRefreshEndpoint)) {
      console.log("Refresh token request!");
      if(auth.CurrentRefreshToken == null)
      {
          this.inj.get(Router).navigate(['/login']);
      }
      return next.handle(request);

    }
    else if (request.url.includes(auth.LoginEndpoint)) {
      console.log("Login request!");
      return next.handle(request);
    }

    if (auth.CurrentAccessToken == null && auth.CurrentRefreshToken != null) {
      return auth.newRefreshAccessToken().mergeMap((p: boolean) => {
        /*if(p === false) {
          this.inj.get(Router).navigate(['/login']);
        }*/

        if(auth.CurrentAccessToken == null && auth.CurrentRefreshToken == null)
        {
            this.inj.get(Router).navigate(['/login']);
            return next.handle(request);
        }

        request = request.clone({
          setHeaders: {
            Authorization: 'Bearer '+ auth.CurrentAccessToken.token
          }
        });


        return next.handle(request);
      });
    }

    if(auth.CurrentAccessToken == null && auth.CurrentRefreshToken == null)
    {
        this.inj.get(Router).navigate(['/login']);
        return next.handle(request);
    }



    request = request.clone({
      setHeaders: {
        Authorization: 'Bearer '+ auth.CurrentAccessToken.token
      }
    });


    return next.handle(request);
  }
}
