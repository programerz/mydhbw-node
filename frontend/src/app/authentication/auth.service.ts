import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/toPromise";
import { ConfigService } from "../config.service";
import { Subject, BehaviorSubject } from "rxjs";
import { promise } from "protractor";

@Injectable()
export class AuthService {
  constructor(private http: HttpClient, private config: ConfigService) {}
  private refreshEvent: Subject<boolean> = new Subject<boolean>();
  private refreshInProgress: boolean = false;

  private currentAccessToken: Token = null;

  public get TokenRefreshEndpoint() {
    return "api/account/token/renew";
  }

  public get LoginEndpoint() {
    return "api/account/login";
  }

  public async login(username: string, password: string) {
    let tokens: Tokens = await this.http
      .post<Tokens>(this.config.RootUrl + this.LoginEndpoint, {
        username: username,
        password: password
      })
      .toPromise()
      .catch(x => {
        return null;
      });

    if (tokens) {
      this.currentAccessToken = tokens.access;
      this.CurrentRefreshToken = tokens.refresh;
    }
  }

  public async logout() {
    await this.http
      .post(
        this.config.RootUrl + "api/account/logout",
        this.CurrentRefreshToken
      )
      .toPromise()
      .catch(x => {
        return null;
      });
    this.CurrentRefreshToken = null;
    this.currentAccessToken = null;
  }

  public newRefreshAccessToken(): Observable<boolean> {
    if (this.CurrentRefreshToken != null) {
      if (this.refreshInProgress === false) {
        this.refreshInProgress = true;
        return this.http
          .post<Tokens>(
            this.config.RootUrl + this.TokenRefreshEndpoint,
            this.CurrentRefreshToken
          )
          .catch(k => {
            this.currentAccessToken = null;
            this.CurrentRefreshToken = null;
            this.refreshEvent.next(false);
            this.refreshInProgress = false;
            return Observable.of<boolean>(false);
          })
          .mergeMap((p: Tokens) => {
            if (p) {
              this.currentAccessToken = p.access;
              this.CurrentRefreshToken = p.refresh;
              this.refreshEvent.next(true);
              this.refreshInProgress = false;
              return Observable.of<boolean>(true);
            } else {
              this.currentAccessToken = null;
              this.CurrentRefreshToken = null;
              this.refreshEvent.next(false);
              this.refreshInProgress = false;
              return Observable.of<boolean>(false);
            }
          });
      } else {
        return this.refreshEvent;
      }
    } else {
      this.currentAccessToken = null;
      this.CurrentRefreshToken = null;
      return Observable.of<boolean>(false);
    }
  }

  /* public async refreshAccessToken(): Promise<string> {
    if (this.CurrentRefreshToken != null) {
      console.log(this.refreshInProgress);
      if (this.refreshInProgress === false) {
        this.refreshInProgress = true;

        let tokens = await this.http
          .post<Tokens>(
            this.config.RootUrl + this.TokenRefreshEndpoint,
            this.CurrentRefreshToken
          )
          .toPromise()
          .catch(x => {
            return null;
          });

        if (tokens) {
          this.currentAccessToken = tokens.access;
          this.CurrentRefreshToken = tokens.refresh;
        } else {
          this.currentAccessToken = null;
          this.CurrentRefreshToken = null;
        }
        this.refreshEvent.next("Done");
        this.refreshInProgress = false;
        return "Done";
      } else {
        console.log("Waiting for request!");
        this.refreshEvent.subscribe(k => console.log("FIRED"));
        await this.refreshEvent.toPromise();
        console.log("Waited for refresh!");
        return "Done";
      }
    } else {
      this.currentAccessToken = null;
      this.CurrentRefreshToken = null;
    }
    return "Done";
  } */

  public get CurrentAccessToken() {
    if (this.currentAccessToken != null) {
      let diffMs = +new Date(this.currentAccessToken.expiration) - +new Date();
      if (diffMs <= 0) {
        return null;
      }
      return this.currentAccessToken;
    }
    return null;
  }

  public get CurrentRefreshToken(): Token {
    if (localStorage.getItem("RefreshToken")) {
      let token: Token = JSON.parse(localStorage.getItem("RefreshToken"));

      let diffMs = +new Date(token.expiration) - +new Date();
      if (diffMs <= 0) {
        localStorage.clear();
        return null;
      }
      return token;
    }
    return null;
  }

  public set CurrentRefreshToken(input: Token) {
    if (input != null) {
      localStorage.setItem("RefreshToken", JSON.stringify(input));
    } else {
      localStorage.clear();
    }
  }
}

class Token {
  public token: string;
  public expiration: Date;
}

class Tokens {
  public access: Token;
  public refresh: Token;
}
