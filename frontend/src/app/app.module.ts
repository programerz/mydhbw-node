import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { CalendarModule } from "ap-angular2-fullcalendar";
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AuthGuard } from './authentication/auth.guard';
import {AuthService} from './authentication/auth.service';
import {AuthInterceptor} from './authentication/auth.interceptor';
import {ConfigService} from './config.service';
import {FileManagementService} from './services/file-management.service'

import { AppComponent } from './app.component';
import { ChatComponent } from './chat/chat.component';
import { CalendarComponent } from './calendar/calendar.component';
import { ScriptsComponent } from './scripts/scripts.component';
import { GameComponent } from './game/game.component';
import { LoginComponent } from './login/login.component';

const appRoutes: Routes = [
  { path: 'calendar', component: CalendarComponent, canActivate: [AuthGuard] },
  { path: 'scripts', component: ScriptsComponent, canActivate: [AuthGuard] },
  { path: 'game', component: GameComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  //{ path: '', component: CalendarComponent, canActivate: [AuthGuard]},
  { path: '**', redirectTo: '/calendar', pathMatch: 'full' }



];

@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    CalendarComponent,
    ScriptsComponent,
    GameComponent,
    LoginComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    CalendarModule
  ],
  providers: [AuthGuard, AuthService, ConfigService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  FileManagementService],

  bootstrap: [AppComponent]
})

export class AppModule { }
