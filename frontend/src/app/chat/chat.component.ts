import { Component, AfterViewInit, NgZone, ViewChild } from "@angular/core";
import { ConfigService } from "../config.service";
import { AuthService } from "../authentication/auth.service";

import * as io from "socket.io-client";

declare function tdfw(): any;
declare function flip(): any;
declare function ilove(): any;

@Component({
  selector: "app-chat",
  templateUrl: "./chat.component.html",
  styleUrls: ["./chat.component.css"]
})
export class ChatComponent implements AfterViewInit {
  public isChatCollapsed = true;
  chat: Array<message>;
  users: Array<user>;
  userHasName: boolean;
  username: string = "";
  hubConnected: boolean = false;
  baseURL: string;
  private socket: SocketIOClient.Socket;

  zone: NgZone;
  @ViewChild("chatHistory") chatHistory;

  constructor(
    private configService: ConfigService,
    private authService: AuthService
  ) {
    this.zone = new NgZone({ enableLongStackTrace: false });
    this.userHasName = false;
    this.chat = new Array();
    this.users = new Array();
  }

  async ngAfterViewInit() {

    if (this.authService.CurrentAccessToken == null) {
      await this.authService.newRefreshAccessToken().toPromise();
    }

    this.baseURL = this.configService.RootUrl + "/chat" + "?token=";
    this.baseURL = this.baseURL + this.authService.CurrentAccessToken.token;

    this.startConnection();
    this.registerOnServerEvents();

    // this.socket..onclose(k => {
    //   this.ngAfterViewInit();
    // });
  }

  public sendChatMessage(event: any) {
    this.socket.emit("SendMessage", {
      user: this.username,
      payload: event.target.value
    });
    event.target.value = "";
  }

  public sendUserName() {
    this.socket.emit("UserJoin", this.username);
    this.userHasName = true;
  }

  private registerOnServerEvents(): void {
    this.socket.on("SetUsersOnline", (data: Array<string>) => {
      this.zone.run(() => {
        const tmp = new Array<user>();
        data.forEach(k => tmp.push(new user(k)));
        this.users = tmp;
      });
    });

    this.socket.on("ReceiveMessage", (data: message) => {
      this.zone.run(() => {
        if (!this.checkIfCommand(data.payload)) {
          this.chat.push(data);
        }
      });
      this.chatHistory.nativeElement.scrollTop = this.chatHistory.nativeElement.scrollHeight;
    });
  }

  private startConnection(): void {
    try {
      this.socket = io(this.baseURL);
      console.log("Hub connection started");
      this.hubConnected = true;
    } catch (error) {
      console.log("Error while establishing connection");
    }
  }

  private checkIfCommand(message: string): boolean {
    if (message.substring(0, 3) === "!!!") {
      const stringToSplit = message;
      const splittedCommand = stringToSplit.split(" ");

      if (splittedCommand[1] === undefined) {
        this.runComand(splittedCommand[0]);
      } else {
        if (splittedCommand[1] === this.username) {
          this.runComand(splittedCommand[0]);
        }
      }
      return true;
    } else {
      return false;
    }
  }
  private runComand(command: string): void {
    if (this.userHasName == false) {
      return;
    }

    switch (command) {
      case "!!!tdfw": {
        tdfw();
        break;
      }
      case "!!!flip": {
        flip();
        break;
      }
      case "!!!ilove": {
        ilove();
        break;
      }
      default: {
        break;
      }
    }
  }
}

export class message {
  payload: string;
  user: string;
  time: string;
  constructor(payload: string, user: string, time: string) {
    this.payload = payload;
    this.user = user;
    this.time = time;
  }
}

export class user {
  name: string;
  constructor(name: string) {
    this.name = name;
  }
}
