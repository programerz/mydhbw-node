import { Component, OnInit, ViewChild } from '@angular/core';
import { FileManagementService } from '../services/file-management.service';
import { FileManagementItem } from '../models/file-management-item.model';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-scripts',
  templateUrl: './scripts.component.html',
  styleUrls: ['./scripts.component.css']
})
export class ScriptsComponent implements OnInit {
  @ViewChild('fileInput') fileInput;
  @ViewChild('folderInput') folderInput;

  path: string[] = new Array<string>();
  items: FileManagementItem[] = new Array<FileManagementItem>();
  refresh: Boolean = false;

  constructor(private fileservice: FileManagementService, private modalService: NgbModal) {
    this.refreshList();
  }

  ngOnInit() {
    this.refreshList();
  }



  //#region navigation
  goBack(num: number) {
    var tmpcalc = this.path.length - num;
    for (var index = 0; index < tmpcalc; index++) {
      this.path.pop();
    }

    this.refreshList();
  }

  goHome() {
    this.path.length = 0;
    this.refreshList();
  }

  /** Gets the current path of the current folder */
  getCurrentPath(): string {
    let tmp = "";
    if (this.path.length == 0) {
      return "";
    }
    this.path.forEach(element => {
      tmp += element + "/";
    });

    return tmp;
  }

  /** Navigates to specific folder or triggers file download */
  itemClick(item: FileManagementItem) {
    if (FileManagementItem.isFolder(item)) {
      this.path.push(item.name);
      this.refreshList();
    }
    else {
      this.fileservice.GetFile(FileManagementItem.getPath(item))
    }
  }

  //#endregion

  /** Delete a Item */

  open(content, item, event) {
    event.stopPropagation();
    this.modalService.open(content).result.then((result) => {
      this.deleteItem(item);
    },(reason)=>{});
  }

  async deleteItem(item: FileManagementItem) {

    if (FileManagementItem.isFolder(item)) {
      await this.fileservice.DeleteFolder(FileManagementItem.getPath(item))
    }
    else {
      await this.fileservice.DeleteFile(FileManagementItem.getPath(item))
    }
    this.refreshList();
  }

  /** Create a new folder */
  async createFolder() {
    let folder = this.folderInput.nativeElement;
    if (folder && folder.value != "") {
      await this.fileservice.AddFolder(folder.value, this.getCurrentPath());
      folder.value = "";
      this.refreshList();
    }

  }

  async refreshList() {
    this.refresh = false;
    this.items = await this.fileservice.GetFolder(this.getCurrentPath());
    this.refresh = true;
  }

  //#region FileUpload
  async upload() {
    let fileBrowser = this.fileInput.nativeElement;
    if (fileBrowser.files && fileBrowser.files[0]) {
      const formData = new FormData();
      formData.append("file", fileBrowser.files[0]);
      await this.fileservice.UploadFile(formData, this.getCurrentPath());
      fileBrowser.value = "";
      this.refreshList();
    }
  }
  //#endregion

  //#region passthrought for item operations
  isFolder(item: FileManagementItem): boolean {
    return FileManagementItem.isFolder(item);
  }
  getSize(item: FileManagementItem): string {
    return FileManagementItem.getSize(item);
  }
  getPath(item: FileManagementItem): string {
    return FileManagementItem.getPath(item);
  }
  //#endregion


}

