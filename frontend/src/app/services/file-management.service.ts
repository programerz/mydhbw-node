import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { FileManagementItem } from '../models/file-management-item.model'
import { ConfigService } from '../config.service';
import * as FileSaver from 'file-saver';
import { Observable } from "rxjs/Rx";
import 'rxjs/add/operator/toPromise';

/** File Management Service */
@Injectable()
export class FileManagementService {

  /** Default ctor */
  constructor(private http: HttpClient, private config: ConfigService) { }

  /** Get the folder with the given path from the backend */
  public async GetFolder(path: string) {
    let tmp = await this.http.get<FileManagementItem[]>(this.config.RootUrl + "api/filemanagement/folder/" + path).toPromise().catch(k=>{return new Array<FileManagementItem>()});
    return tmp;
  }

  /** Get the file with the given path from backend */
  public async GetFile(path: string) {
    var tmp = await this.http.get(this.config.RootUrl + "api/filemanagement/file/" + path,
      { observe: 'response', responseType: 'blob', headers: new HttpHeaders().set('Access-Control-Expose-Headers', 'content-disposition') })
      .toPromise().catch(k=>{return null;});
    if(tmp != null)
    {
    FileSaver.saveAs(tmp.body, this.getFileNameFromContentdispositionHeader(tmp.headers.get('content-disposition')));
    }
    /*
     //This open the blob if its a pdf file within the browser, only works when add blocker not on
        if(tmp.body.type =="application/pdf")
        {
          let fileURL = window.URL.createObjectURL(tmp.body);
          window.open(fileURL);
        }else{
         FileSaver.saveAs(tmp.body,this.getFileNameFromContentdispositionHeader(tmp.headers.get('content-disposition')));
        }*/
  }

  /** Send given file to the backend, save it there with the given path */
  public async UploadFile(formData: FormData, path: string) {
    await this.http.post(this.config.RootUrl + "api/filemanagement/file/" + path, formData)
    .toPromise()
    .catch(k=>{return;});
  }

  /** Creates a new folder with the given name on the given path */
  public async AddFolder(folderName: string, path: string) {
    await this.http.post(this.config.RootUrl + "api/filemanagement/folder/" + path + folderName, "")
    .toPromise()
    .catch(k=>{return;});
  }

  public async DeleteFolder(path: string)
  {
    await this.http.delete(this.config.RootUrl + "api/filemanagement/folder/" + path)
    .toPromise()
    .catch(k=>{return;});
  }

  public async DeleteFile(path:string)
  {
    await this.http.delete(this.config.RootUrl + "api/filemanagement/file/" + path)
    .toPromise()
    .catch(k=>{return;});
  }


  private getFileNameFromContentdispositionHeader(header: string) {
    var result = header.split(';')[1].trim().split('=')[1];
    return result.replace(/"/g, '');
  }

}
