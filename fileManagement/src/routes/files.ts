import { Router, Request, Response } from "express";
import { FileManagementService } from "../services/fileManagementService";
import { AccountService } from "../services/accountService";
import { Z_TREES } from "zlib";
import { UploadedFile } from "express-fileupload";

const router: Router = Router();
const accountService = new AccountService();

router.get("/folder/*", (req: Request, res: Response) => {
  var group = accountService.getGroup(<string>req.header("Authorization"));
  if (group === undefined) {
    res.statusCode = 404;
    res.send();
  } else {
    var tmp = new FileManagementService(group);
    let path = "";
    if (req.params["0"]) {
      path = decodeURI(req.params["0"]);
    }

    let tmp2 = tmp.GetFolder(path);
    if (tmp.HasErrors()) {
      res.statusCode = 400;
      res.json(tmp.GetErrors());
    } else if (tmp2 === undefined) {
      res.statusCode = 404;
      res.send();
    } else {
      res.statusCode = 200;
      res.json(tmp2);
    }
  }
});


router.post("/folder/*", (req: Request, res: Response) => {
  var group = accountService.getGroup(<string>req.header("Authorization"));
  if (group === undefined) {
    res.statusCode = 404;
    res.send();
  } else {
    var tmp = new FileManagementService(group);
    let path = "";
    if (req.params["0"]) {
      path = decodeURI(req.params["0"]);
    }

    if (path === "") {
      res.statusCode = 404;
      res.send();
    } else {
      tmp.CreateFolder(path);
      if (tmp.HasErrors()) {
        res.statusCode = 400;
        res.json(tmp.GetErrors());
      }else {
        res.statusCode = 204;
        res.send();
      }
    }
  }
});


router.delete("/folder/*", (req: Request, res: Response) => {
  var group = accountService.getGroup(<string>req.header("Authorization"));
  if (group === undefined) {
    res.statusCode = 404;
    res.send();
  } else {
    var tmp = new FileManagementService(group);
    let path = "";
    if (req.params["0"]) {
      path = decodeURI(req.params["0"]);
    }

    if (path === "") {
      res.statusCode = 400;
      res.send("Root directory cannot be deleted");
    } else {
      tmp.DeleteFolder(path);
      if (tmp.HasErrors()) {
        res.statusCode = 400;
        res.json(tmp.GetErrors());
      }else {
        res.statusCode = 204;
        res.send();
      }
    }
  }
});

router.get("/file/*", (req: Request, res: Response) => {
  var group = accountService.getGroup(<string>req.header("Authorization"));
  if (group === undefined) {
    res.statusCode = 404;
    res.send();
  } else {
    var tmp = new FileManagementService(group);
    let path = "";
    if (req.params["0"]) {
      path = decodeURI(req.params["0"]);
    }

    if (path === "") {
      res.statusCode = 404;
      res.send();
    } else {
      var filePath = tmp.GetFile(path);
      if (tmp.HasErrors()) {
        res.statusCode = 400;
        res.json(tmp.GetErrors());
      }else {
        res.statusCode =200;
        res.download(<string>filePath);
      }
    }
  }
});


router.post("/file/*", (req: Request, res: Response) => {
  if (!req.files) {
    return res.status(400).send("No files were uploaded.");
  }
  var group = accountService.getGroup(<string>req.header("Authorization"));
  if (group === undefined) {
    res.statusCode = 404;
    res.send();
  } else {
    var tmp = new FileManagementService(group);
    let path = "";
    if (req.params["0"]) {
      path = decodeURI(req.params["0"]);
    }

    tmp.AddFile(path,<UploadedFile>req.files.file);

    if (tmp.HasErrors()) {
      res.statusCode = 400;
      res.json(tmp.GetErrors());
    }else {
      res.statusCode = 204;
      res.send();
    }
  }


});


router.delete("/file/*", (req: Request, res: Response) => {
  var group = accountService.getGroup(<string>req.header("Authorization"));
  if (group === undefined) {
    res.statusCode = 404;
    res.send();
  } else {
    var tmp = new FileManagementService(group);
    let path = "";
    if (req.params["0"]) {
      path = decodeURI(req.params["0"]);
    }

    if (path === "") {
      res.statusCode = 404;
      res.send();
    } else {
      tmp.DeleteFile(path);
      if (tmp.HasErrors()) {
        res.statusCode = 400;
        res.json(tmp.GetErrors());
      }else {
        res.statusCode = 204;
        res.send();
      }
    }
  }
});

export const FilesController: Router = router;
