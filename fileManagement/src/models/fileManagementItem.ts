import * as node_path from 'path';


export class FileManagementItemFactory {
    public static  Folder (folderName: string, path: string, itemCount: number): FileManagementItem {
        let tmp = new FileManagementItem();
        tmp.name = folderName;
        tmp.path = this.ConvertPath(path);
        tmp.extension = "/";
        tmp.size = itemCount;
        return tmp;
    }
    
    public static File (fileName: string, extension: string, path: string, fileSize: number): FileManagementItem {
        let tmp = new FileManagementItem();
        tmp.name = fileName;
        tmp.extension = extension;
        tmp.path = this.ConvertPath(path);
        tmp.size = fileSize;

        return tmp;
    }

    private static ConvertPath(path: string): string {
        if ((node_path.sep == '\\')) {
            let tmp = path.replace('\\', '/');
            if (tmp.startsWith("/")) {
                tmp = tmp.substr(1);
            }
            
            return tmp;
        }
        
        return path;
    }
}

export class FileManagementItem {
    
    name!: string;
    extension!: string;
    path!: string;
    size!: number;
}
