import { IFileManagementService } from "../interfaces/IFileManagementService";
import { FileManagementItem, FileManagementItemFactory } from "../models/fileManagementItem";
import * as file_node from "fs";
import * as app_root_path from "app-root-path";
import * as path_node from "path";
import { UploadedFile } from "express-fileupload"

export class FileManagementService implements IFileManagementService {

    public constructor(groupName: string) {
        this.groupName = groupName;
    }
    private groupName: string;

    private static filePath: string = path_node.join(app_root_path.path, "data", "FileManagement");

    private readonly errors: Array<string> = new Array<string>();

    private static GetPath(path: string): string {
        return path_node.join(...path.split('/'));
    }

    private GetGroupFolderPath(): string {
        let tmp: string = path_node.join(FileManagementService.filePath, this.groupName);
        if (!file_node.existsSync(tmp)) {
            file_node.mkdirSync(tmp);
        }
        return tmp;
    }

    private GetFileItem(filePath: string): FileManagementItem {
        let pa = path_node.parse(filePath);

        let name = pa.name;
        let extension = pa.ext.substr(1);
        let path = pa.dir.replace(this.GetGroupFolderPath(),"");
        let size = file_node.lstatSync(filePath).size;

        return FileManagementItemFactory.File(name,extension,path,size);

    }

    private GetFolderItem(folderPath: string) {
        let pa = path_node.parse(folderPath);

        let name = pa.name;
        let path = pa.dir.replace(this.GetGroupFolderPath(),"");
        let count = (file_node.readdirSync(folderPath).filter(f => file_node.lstatSync(path_node.join(folderPath, f)).isFile())).length 
        + (file_node.readdirSync(folderPath).filter(f => file_node.lstatSync(path_node.join(folderPath, f)).isDirectory())).length;

        return FileManagementItemFactory.Folder(name,path,count);
    }


    HasErrors(): boolean {
        return this.errors.length > 0 ? true : false;
    }
    GetErrors(): string[] {
        return this.errors;
    }
    GetFolder(path: string): Array<FileManagementItem> | undefined {
        let tmp = path_node.join(this.GetGroupFolderPath(), FileManagementService.GetPath(path));
        if (file_node.existsSync(tmp)) {
            if (file_node.lstatSync(tmp).isFile()) {
                this.errors.push("This is not a Drectory, its a File");
                return undefined;
            }
            
            let dirs = file_node.readdirSync(tmp).filter(f => file_node.lstatSync(path_node.join(tmp, f)).isDirectory())
            let files = file_node.readdirSync(tmp).filter(f => file_node.lstatSync(path_node.join(tmp, f)).isFile())

            let items: Array<FileManagementItem> = new Array<FileManagementItem>();

            dirs.forEach(element => {
                items.push(this.GetFolderItem(path_node.join(tmp,element)));
            });
            
            files.forEach(element => {
                items.push(this.GetFileItem(path_node.join(tmp,element)));
            });

            return items;

        } else {
            return undefined;
        }
    }
    GetFile(path: string): string | undefined {
        var tmp:string = path_node.join(this.GetGroupFolderPath(),path);
        if(file_node.existsSync(tmp)){
            if(file_node.lstatSync(tmp).isDirectory()){
                this.errors.push("This is not a file, its a directory");
                return undefined;
            }
            else {
                return tmp;
            }
        } else {
            this.errors.push("File does not exists");
        }
    }

    CreateFolder(path: string): void {
        var tmp: string = path_node.join(this.GetGroupFolderPath(),path);
        if(file_node.existsSync(tmp)){
            this.errors.push("Folder already exists");
            return;
        } else {
            try {
                file_node.mkdirSync(tmp);
            } catch (e) {
                this.errors.push(e);
            }
        }
    }

    DeleteFolder(path: string): void {
        var tmp: string = path_node.join(this.GetGroupFolderPath(),path);
        if(file_node.existsSync(tmp))
        {
            try{
                file_node.rmdirSync(tmp);
            } catch (e) {
                this.errors.push(e);
            }
        }
        else {
            this.errors.push("Folder does not exists");
        }
    }
    AddFile(path: string, file: UploadedFile): void {
        var tmp: string = path_node.join(this.GetGroupFolderPath(),path,file.name);
        file.mv(tmp,(err: any) => {
            this.errors.push(err);
        });
    }

    DeleteFile(path: string): void {
        var tmp: string = path_node.join(this.GetGroupFolderPath(),path);
        if(file_node.existsSync(tmp))
        {
            try{
                file_node.unlinkSync(tmp);
            } catch (e) {
                this.errors.push(e);
            }
        }
        else {
            this.errors.push("File does not exists");
        }
    }
}