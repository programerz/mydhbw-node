import jwt from 'jsonwebtoken';
//import { Tokens } from '../models/tokens';

export class AccountService {
  config = require('../../data/config');
  constructor() {}

  /**
   *
   *
   * @param {string} access
   * @returns {boolean}
   * @memberof AccountService
   */
  validateToken(access: string): boolean {
    try {
      jwt.verify(access, this.config.Tokens.Key,{ignoreExpiration: false});
    } catch {
      return false;
    }
    return true;
  }

  getGroup(token: string):string | undefined {
    token = token.replace("Bearer ", "");
    token = token.replace("bearer ","");
    token = token.replace("BEARER ","");
    try { 
      var jwt2 = require('jsonwebtoken');
      let tmp = jwt2.decode(token);
        return <string>tmp.sub;
    } catch {
    return undefined;
  }
}
}
