import express from "express";
import bodyparser from "body-parser";

import { FilesController } from "./routes";

import { AccountService } from "./services/accountService";
import fileUpload from "express-fileupload";
import cors from "cors";

const app: express.Application = express();
const port: number = 8080;
const contextPath: string = "/api";
const accountService = new AccountService();

app.use(contextPath, (req, res, next) => {
  let authorized = false;
  let tmp = <string>req.header("Authorization");

  if (tmp) {
    if (accountService.validateToken(tmp.replace("Bearer ", ""))) {
      authorized = true;
    }
  }
  if (authorized) {
    next();
  } else {
    console.log(`Unauthorized request from ${req.ip}`);
    res.sendStatus(401);
  }
});

app.disable("etag");
app.use(cors({exposedHeaders: ["Content-Disposition","content-disposition"]}));
//app.options("*", cors({ exposedHeaders: ["Content-Disposition"] })); // include before other routes
app.use(bodyparser.json());
app.use(fileUpload());
app.use(contextPath + "/filemanagement", FilesController);

app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}/`);
});
