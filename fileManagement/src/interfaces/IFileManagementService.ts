import { FileManagementItem } from "../models/fileManagementItem";
import * as file_node from "fs";
import { UploadedFile } from "express-fileupload"

/**
 * Interface of the FileManagementService
 *
 * @export
 * @interface IFileManagementService
 */
export interface IFileManagementService {
    /**
     * Determines whether this instance has errors.
     *
     * @returns {boolean} true if this instance has errors; otherwise, false
     * @memberof IFileManagementService
     */
    HasErrors(): boolean;

    /**
     * Gets the errors.
     *
     * @returns {Array<string>}
     * @memberof IFileManagementService
     */
    GetErrors(): Array<string>;

    /**
     * Gets the folder.
     *
     * @param {string} path The Path
     * @returns {Array<FileManagementItem>}
     * @memberof IFileManagementService
     */
    GetFolder(path: string): Array<FileManagementItem> | undefined;

    /**
     * Gets the file.
     *
     * @param {string} path The Path
     * @returns {File}
     * @memberof IFileManagementService
     */
    GetFile(path: string): string | undefined;

    /**
     * Creates the folder.
     *
     * @param {string} path The path
     * @memberof IFileManagementService
     */
    CreateFolder(path: string): void;


    /**
     * Delets the folder.
     *
     * @param {string} path The path.
     * @memberof IFileManagementService
     */
    DeleteFolder(path: string): void;

    /**
     * Adds the file.
     *
     * @param {string} path The path.
     * @param {File} file The file.
     * @memberof IFileManagementService
     */
    AddFile(path: string, file: UploadedFile): void;

    /**
     * Deletes the file.
     *
     * @param {string} path The path.
     * @memberof IFileManagementService
     */
    DeleteFile(path: string): void;
}